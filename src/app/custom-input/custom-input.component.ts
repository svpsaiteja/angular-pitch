import { Component, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { tap } from 'rxjs/operators';

const CUSTOM_INPUT_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CustomInputComponent),
  multi: true
}

@Component({
  selector: 'app-custom-input',
  templateUrl: './custom-input.component.html',
  styleUrls: ['./custom-input.component.scss'],
  providers: [CUSTOM_INPUT_VALUE_ACCESSOR]
})
export class CustomInputComponent implements ControlValueAccessor, OnInit {

  constructor() { }

  customInput: FormControl;

  onChange: (value) => void;
  onTouched: () => void;


  writeValue(obj: any): void {
    this.customInput.setValue(obj);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.customInput.disable();
  }

  ngOnInit(): void {

    this.customInput = new FormControl();

    this.customInput.valueChanges
      .pipe(
        tap(value => this.onChange && this.onChange(value))
      ).subscribe();
  }

}
